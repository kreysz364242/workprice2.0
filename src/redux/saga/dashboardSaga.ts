import { all, call, put, takeEvery } from 'redux-saga/effects'
import IAction from '../../models/IAction'
import {postDataToApi, getDataFromApi} from '../../client/requestToAPI'

import{
    ADD_CARD,
    ADD_CARD_TO_STATE,
    RECIVE_CARDS,
    RECIVE_CARDS_TO_STATE
} from '../actions/dashboardActions'

export function* addCard(action: IAction){
    try{
        const { data } = yield call(() => postDataToApi('dashboard/addCard', action.payload))
        yield put({type: ADD_CARD_TO_STATE, payload: data})
    }catch(error){
        console.log(error)
    }
}

export function* reciveTasks(){
    try{
        const { data } = yield call(()=> getDataFromApi('dashboard/getCards'))
        yield put({type: RECIVE_CARDS_TO_STATE, payload: data})
    }catch(error){
        console.log(error)
    }
}

export function* watchReciveTasks(){
    yield takeEvery(RECIVE_CARDS, reciveTasks)
}

export function* watchAddCard(){
    yield takeEvery(ADD_CARD, addCard)
}