import  IAction  from '../../models/IAction'
import initialDashboard from '../../models/components/Dashboard/initialDashboard'
import IDashboard from '../../models/components/Dashboard/IDashboard'
import {
    ADD_CARD_TO_STATE,
    RECIVE_CARDS_TO_STATE
} from '../actions/dashboardActions'

export default function dashboard(
    state = initialDashboard,
    {type , payload} : IAction

) : IDashboard {

    switch(type){  
        case ADD_CARD_TO_STATE: {
            const newDashboardData = {...state.dashboardData} 
            newDashboardData.columns[payload.column].tasksIds = [...newDashboardData.columns[payload.column].tasksIds, `${payload.id}`]
            console.log(newDashboardData)
            return {...state}
        }  
        case RECIVE_CARDS_TO_STATE: {
            const newDashboardColumns = {...state.dashboardData.columns}
            let newCards = {}
            console.log(state.dashboardData.cards)
            payload.forEach((card: any) => (newCards as any)[`${card.id}`] = card)
            console.log(newCards)
            payload.forEach((card: any) => newDashboardColumns[card.column].cardsIds = [...newDashboardColumns[card.column].cardsIds, `${card.id}`])
            console.log(newDashboardColumns)
            return {...state}
        }
    }
    return state
}