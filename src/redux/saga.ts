import { all } from 'redux-saga/effects'

import{
    watchAddCard,
    watchReciveTasks
} from './saga/dashboardSaga'

export default function* saga(){
    yield all([
        watchAddCard(),
        watchReciveTasks()
    ])
}