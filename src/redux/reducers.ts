import { combineReducers } from "redux";
import { connectRouter } from 'connected-react-router'
import dashboard from "./reducers/dashboardReducer";



const createRootReducer = (history : any) => combineReducers({
    router: connectRouter(history),
    dashboard
  })

export default createRootReducer