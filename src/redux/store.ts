import {createStore, compose, applyMiddleware} from 'redux'
import { createBrowserHistory } from 'history'
import createRootReducer from './reducers'
import { routerMiddleware } from 'connected-react-router'
import createSagaMiddleware from 'redux-saga'
import saga from './saga'


export const history = createBrowserHistory()

const sagaMiddleware = createSagaMiddleware()
const composeEnhancer = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
export const store = createStore(createRootReducer(history), composeEnhancer(applyMiddleware(routerMiddleware(history),sagaMiddleware)))
sagaMiddleware.run(saga)
