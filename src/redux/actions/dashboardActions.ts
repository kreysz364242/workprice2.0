import { IModalData } from '../../components/dashboard/addModal/addModal'
import IAction from '../../models/IAction'

export const ADD_CARD = 'ADD_CARD'
export const ADD_CARD_TO_STATE = 'ADD_CARD_TO_STATE'
export const RECIVE_CARDS = 'RECIVE_CARDS'
export const IS_RECIVE_CARDS = 'IS_RECIVE_CARDS'
export const RECIVE_CARDS_TO_STATE = 'RECIVE_CARDS_TO_STATE'

export const addCard = (payload: IModalData): IAction =>({
    payload,
    type: ADD_CARD
})

export const reciveCards = (): IAction =>({
    type: RECIVE_CARDS
})

export const isReciveCards = (): IAction =>({
    type: IS_RECIVE_CARDS
})