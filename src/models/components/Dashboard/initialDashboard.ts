import IDashboard from './IDashboard'

export default {
    dashboardData: {
        cards: {
            'task-1': { id: 'task-1', content: 'Take out of' },
            'task-2': { id: 'task-2', content: 'Take out of' },
            'task-3': { id: 'task-3', content: 'Take out of' },
            'task-4': { id: 'task-4', content: 'Take out of1' },
            'task-5': { id: 'task-5', content: 'Take out of' },
        },

        columns: {
            'Closed': {
                id: 'Closed',
                title: 'Closed',
                cardsIds: []
            },

            'Waiting for a response': {
                id: 'Waiting for a response',
                title: 'Waiting for a response',
                cardsIds: []
            },

            'hired': {
                id: 'hired',
                title: 'hired',
                cardsIds: []
            },
            'In discussion': {
                id: 'In discussion',
                title: 'In discussion',
                cardsIds: []
            },

            'In review':{
                id: 'In review',
                title: 'In review',
                cardsIds: []
            }

        },

        columnOrder: ['Closed', 'Waiting for a response', 'hired', 'In discussion', 'In review'],
    },

    isReciveCards: false
} as IDashboard