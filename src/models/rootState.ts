import IDashboard from './components/Dashboard/IDashboard';

export default interface IRootState{
    dashboard: IDashboard
}