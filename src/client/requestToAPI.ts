import axios from 'axios'

export const postDataToApi = (route: string,  data: any | null, token: string = '') =>{
    return axios.post(`${process.env.REACT_APP_API_ADRESS}${route}`, {data}, { headers: { Authorization: `Bearer ${token}` } })
}

export const getDataFromApi = (route: string, params: string = '') =>{
    return axios.get(`${process.env.REACT_APP_API_ADRESS}${route}${params}`)
}