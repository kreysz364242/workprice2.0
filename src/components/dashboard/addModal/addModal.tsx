import React, { useState } from 'react'
import { addCard, isReciveCards } from '../../../redux/actions/dashboardActions'
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Form,
  FormGroup,
  Label,
  Input
} from 'reactstrap';
import { connect } from 'react-redux';
import IAction from '../../../models/IAction';



export interface IModalData {
  vacancy: string,
  customerRate: number,
  technologyStack: string,
  approximateDuration: string,
  whoFound: string,
  column: string,
  isFit: string,
  rejectionReason: string,
  cover: string
}

interface IAddCardModal {
  isModalOpen: boolean,
  toggleModal: () => void
  addCard: (data: IModalData) => IAction
}

const AddCardModal = (props: IAddCardModal) => {
  const { isModalOpen, toggleModal, addCard } = props
  const [modalInputData, setModalData] = useState({
    vacancy: '',
    customerRate: 0,
    technologyStack: '',
    column: 'Closed',
    approximateDuration: '',
    whoFound: 'Царское величество Артем',
    isFit: 'yes',
    rejectionReason: '',
    cover: ''
  } as IModalData)

  const changeInputData = (modalDataFiled: string, dataParam: any) => {
    console.log(modalInputData)
    setModalData((data: IModalData) => ({
      ...data, [modalDataFiled]: dataParam
    }))
  }

  const validationFieldsAndSendData = () => {
    if(
      modalInputData.vacancy !== ''
      && modalInputData.whoFound !== ''
      && modalInputData.isFit !== ''
    ) {
      toggleModal()
      addCard(modalInputData)
    }else{
      console.log("validate error")
    }
  }

  return (
    <Modal isOpen={isModalOpen} toggle={toggleModal} >
      <ModalHeader toggle={toggleModal}>Add card</ModalHeader>
      <ModalBody>
        <Form>
          <FormGroup>
            <Label for="vacancy">Vacancy</Label>
            <Input
              onChange={(event: any) => { changeInputData('vacancy', event.target.value) }}
              value={modalInputData.vacancy}
              type="url"
              id="vacancy"
            />
          </FormGroup>
          <FormGroup>
            <Label for="customerRate">Customer rate</Label>
            <Input
              onChange={(event: any) => { changeInputData('customerRate', event.target.value) }}
              value={modalInputData.customerRate}
              type="number"
              id="customerRate"
            />
          </FormGroup>
          <FormGroup>
            <Label for="technologyStack">Technology stack</Label>
            <Input
              onChange={(event: any) => { changeInputData('technologyStack', event.target.value) }}
              value={modalInputData.technologyStack}
              type="text"
              id="technologyStack"
            />
          </FormGroup>
          <FormGroup>
            <Label for="approximateDuration">Approximate duration</Label>
            <Input
              onChange={(event: any) => { changeInputData('approximateDuration', event.target.value) }}
              value={modalInputData.approximateDuration}
              type="text"
              id="approximateDuration"
            />
          </FormGroup>
          <FormGroup>
            <Label for="whoFound">Who found</Label>
            <Input
              value={modalInputData.whoFound}
              onChange={(event: any) => { changeInputData('whoFound', event.target.value) }}
              type="select"
              id="whoFound"
            >
              <option>Царское величество Артем</option>
              <option>Зяблы</option>
            </Input>
          </FormGroup>
          <FormGroup>
            <Label for="isFit">Fit?</Label>
            <Input
              value={modalInputData.isFit}
              onChange={(event: any) => { changeInputData('isFit', event.target.value) }}
              type="select"
              id="isFit"
            >
              <option>yes</option>
              <option>no</option>
              <option>may be</option>
            </Input>
          </FormGroup>
          <FormGroup>
            <Label for="rejectionReason">Rejection reason</Label>
            <Input
              onChange={(event: any) => { changeInputData('rejectionReason', event.target.value) }}
              value={modalInputData.rejectionReason}
              type="text"
              id="rejectionReason" />
          </FormGroup>
          <FormGroup>
            <Label for="cover">Cover</Label>
            <Input
              onChange={(event: any) => { changeInputData('cover', event.target.value) }}
              value={modalInputData.cover}
              type="textarea"
              id="cover" />
          </FormGroup>
        </Form>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={validationFieldsAndSendData}> Do Something</Button>{' '}
        <Button color="secondary" onClick={toggleModal}>Close</Button>
      </ModalFooter>
    </Modal>
  )
}

const mapDispatchToProps = {
  addCard,
  isReciveCards
}

export default connect(null, mapDispatchToProps)(AddCardModal)