import React, { Children } from 'react'
import styled from 'styled-components'
import Task from './card/task'
import { Droppable } from 'react-beautiful-dnd'
import './column.scss'



const getListStyle = (isDraggingOver: boolean) => ({
    background: isDraggingOver ? "lightblue" : "lightgrey",
  });

const Column = (props: any) => {
    console.log(props)
    return (
        <div className='column column-container col-2'>
            <div className='column__title'>{props.column.title}</div>
            <Droppable droppableId={props.column.id}>
                {(provided, snapshot) => (
                    <div
                        {...provided.droppableProps}
                        ref={provided.innerRef}
                        className='column__tasks'
                        style={getListStyle(snapshot.isDraggingOver)}
                    >
                        {props.cards.map((card: any, index: number) =>
                            <Task
                                key={card.id}
                                card={card}
                                index={index}
                            />)}
                        {provided.placeholder}
                    </div>)}

            </Droppable>

        </div>
    )
}

export default Column