import React from 'react'
import './task.scss'
import { Draggable } from "react-beautiful-dnd"




const Task = (props: any) => {


    const margin = 5
    const getItemStyle = (isDragging: boolean, draggableStyle: any) => ({
        // some basic styles to make the items look a bit nicer
        userSelect: "none",
        margin: `0 0 ${margin}px 0`,

        // change background colour if dragging
        background: isDragging ? "lightgreen" : "grey",

        // styles we need to apply on draggables
        ...draggableStyle
    });


    return (
        <Draggable draggableId={props.card.id} index={props.index}>
            {(provided, snapshot) => (
                <div className='task'
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    style={getItemStyle(
                        snapshot.isDragging,
                        provided.draggableProps.style
                    )}
                >
                    {props.card.content}
                </div>
            )}
        </Draggable>

    )
}

export default Task