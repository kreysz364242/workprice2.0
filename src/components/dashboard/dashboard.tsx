import React, { useState, useEffect } from 'react';
import { DragDropContext } from 'react-beautiful-dnd'
import Column from './column/column'
import IRootState from '../../models/rootState';
import { connect } from 'react-redux';
import { Button } from 'reactstrap';
import AddCardModal from './addModal/addModal';
import { reciveCards } from '../../redux/actions/dashboardActions'

const Dashboard = (props: any) => {

    const [data, setData] = useState(props.data)
    const [isModalOpen, setModalOpen] = useState(false)

    useEffect(()=>{
        props.reciveCards()
    }, [])


    const onDragEnd = (result: any) => {
        const { destination, source, draggableId } = result
        if (!destination) return

        if (
            destination.droppableId === source.droppableId
            && destination.index === source.index
        ) return

        const start = (data.columns as any)[source.droppableId]
        const finish = (data.columns as any)[destination.droppableId]

        if(start === finish){
            const column = (data.columns as any)[source.droppableId]
            const newTaskIds = Array.from(column.cardsIds)
            newTaskIds.splice(source.index, 1)
            newTaskIds.splice(destination.index, 0, draggableId)
    
    
            const newColumn = {
                ...column,
                cardsIds: newTaskIds
            }
    
            setData((data: any) => ({
                ...data,
                columns: {
                    ...data.columns,
                    [newColumn.id]: newColumn
                }
            }))

            return
        }

       const startcardsIds = Array.from(start.cardsIds)
       startcardsIds.splice(source.index, 1)
       const newStart = {
           ...start, 
           cardsIds: startcardsIds
       } 

       const finishcardsIds = Array.from(finish.cardsIds)
       finishcardsIds.splice(destination.index, 0, draggableId)
       
       const newFinish = {
        ...finish, 
        cardsIds: finishcardsIds
       }

        setData((data: any) =>({
            ...data, 
            columns:{
                ...data.columns,
                [newStart.id]: newStart,
                [newFinish.id]: newFinish
            }
        }))
     
    }


    const toggleModal = () =>{
        setModalOpen(!isModalOpen)
    }

    return (
        <DragDropContext
            onDragEnd={onDragEnd}
        >
            <div className='container'>
                <div className='row'>
                    {data.columnOrder.map((columnId: any) => {
                        const column: any = (data.columns as any)[columnId]
                        const cards: any = column.cardsIds.map((taskId: any) => (data.cards as any)[taskId])
                        return <Column key={column.id} column={column} cards={cards} />
                    })}
                </div>
                <div className='row'>
                    <Button color="success" onClick={toggleModal}>Add card</Button>
                    <AddCardModal {...{isModalOpen, toggleModal}}/>
                </div>
            </div>

        </DragDropContext>
    )
}

const mapStateToProps = (state: IRootState) =>({
    data: state.dashboard.dashboardData
})

const mapDispatchToProps = {
    reciveCards
}

export default  connect(mapStateToProps, mapDispatchToProps)(Dashboard)