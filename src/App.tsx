import * as React from 'react'
import { Provider } from 'react-redux'
import { Route } from 'react-router'
import { ConnectedRouter, routerActions } from 'connected-react-router'
import { store, history } from './redux/store'
import Dashboard from './components/dashboard/dashboard'


const App = () => {
    return (
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <Route exact path="/" component={Dashboard} />
            </ConnectedRouter>
        </Provider>)
}

export default App